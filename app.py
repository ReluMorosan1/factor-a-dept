from flask import Flask
import json
import itertools
import requests

app = Flask(__name__)

class geeks:
    def __init__(self, name, roll):
        self.name = name
        self.roll = roll



@app.route('/')
def hello_world():
    list = []
    response = requests.get("https://www.reddit.com/.json", headers={'User-agent': 'your bot 0.1'})
    response_data = response.json()
    data = response_data["data"]
    children = data["children"]
    for post in children:
        x = post["data"]
        list.append(geeks(x["title"], x['ups']))
    newlist = sorted(list, key=lambda x: x.roll, reverse=True)
    top5 = itertools.islice(newlist, 5)
    finalData = {}
    finalData['posts'] = []
    k = 1
    for obj in top5:
        finalData['posts'].append({
            'place': k,
            'title': obj.name,
            'upvotes': obj.roll
        })
        k = k + 1
    return finalData


if __name__ == '__main__':
    app.run()